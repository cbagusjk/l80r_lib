#ifndef __L80_R_H
#define __L80_R_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

typedef struct
{
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	uint16_t microseconds;
	double latitude;
	uint8_t ns;
	double longitude;
	uint8_t ew;
	uint8_t fix_status;
	uint8_t satellite_view;
	double hdop;
	double altitude;
	uint8_t alt_unit;
	double geoid_separation;
	uint8_t geoid_unit;
	uint16_t dgps_age;
	uint16_t dgps_id;
	uint8_t checksum;
} gga_data_t;

typedef struct
{
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	uint16_t microseconds;
	uint8_t valid;
	double latitude;
	uint8_t ns;
	double longitude;
	uint8_t ew;
	float speed;
	float heading;
	uint8_t date;
	uint8_t month;
	uint8_t year;
	uint8_t positioning_mode;
	uint8_t checksum;
} rmc_data_t;

typedef struct
{
	gga_data_t gga_data;
	rmc_data_t rmc_data;
} gps_data_t;

void L80R_Init();
void L80R_UART_Callback();
void L80R_DMA_RxMode();
void L80R_UART_RxProcess();

#ifdef __cplusplus
}
#endif

#endif
