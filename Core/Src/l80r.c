/**
  ******************************************************************************
  * @file    l80r.c
  * @author  Cipto Bagus Jati Kusumo (cbagusjk@live.com)
  * @brief   Quectel L80-R module driver.
  *          This file provides firmware functions to manage the following
  *          functionalities of the Quectel L80-R module:
  *           + Initialization
  @verbatim
  ==============================================================================
                        ##### How to use this driver #####
  ==============================================================================
  [..]
   (#) UART and System Ticks must be in different priority (System Ticks higher than UART)
   (#) UART at 9600 baud
   (#) Enable UART Global Interrupt
   (#) Enable DMA Transfer at both TX and RX
   (#) Enable DMA Global Interrupt
   (#) One Pulse Mode at Timers, 5 seconds time length because Quectel L80-R booting time take atleast 3 seconds
   (#) Change UART_HANDLER value depending on your setting
   (#) Change TIMER_HANDLER value depending on your setting
   (#) Change GPS_PWR_GPIOPIN value depending on your setting
   (#) Change GPS_PWR_GPIOPORT value depending on your setting
   (#) Your GPS power must be controlled by Software
   (#) Call L80R_Init() in your main application
   (#) Call L80R_UART_Callback() after HAL_UART_HANDLER() in your UARTx_IRQHandler()
   (#) Call L80R_DMA_RxMode() before HAL_DMA_IRQHandler() in your DMAx_Channelx_IRQHandler() (TX DMA Handler)
   (#) Call L80R_UART_RxProcess() before HAL_DMA_IRQHandler() in your DMAx_Channelx_IRQHandler() (RX DMA Handler)
   (#)
  @endverbatim
*/

/*
 * To do list :
 * (#) Deinit
 * (#) Porting to L411
 *
*/

/* Include header file */
#include "l80r.h"

/* GPS receive buffer */
#define GPS_BUFFER_SIZE 200

/* Micro Controller Unit (MCU) Peripheral */
#define UART_HANDLER huart1
#define TIMER_HANDLER htim2
#define DMATX_HANDLER hdma_usart1_tx
#define DMARX_HANDLER hdma_usart1_rx

/* GPS Power GPIO Pin and Port */
#define GPS_PWR_GPIOPORT LD4_GPIO_Port
#define GPS_PWR_GPIOPIN	LD4_Pin

/* GPS status */
#define L80R_UNINITIALIZE		0
#define L80R_INITIALIZATION		1
#define L80R_ERROR				2
#define L80R_READY				3
#define L80R_RUNNING			4

/* DMA status */
#define L80R_DMADEFAULTMODE		0
#define L80R_DMATXMODE			1
#define L80R_DMARXMODE			2

/* List of PMTK Command aka Initialization Command */
#define l80r_nmeaOff "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n\0"
#define l80r_normalMode "$PMTK886,0*28\r\n\0"
#define l80r_easyMode "$PMTK869,1,1*35\r\n\0"
#define l80r_setOutputPeriode "$PMTK220,600*28\r\n\0"
#define l80r_nmeaOn "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n\0"

/* List of PMTK Reply */
#define l80r_nmeaOff_r "$PMTK001,314,3*36\r\n\0"
#define l80r_normalMode_r "$PMTK001,886,3*36\r\n\0"
#define l80r_easyMode_r "$PMTK001,869,3*37\r\n\0"
#define l80r_setOutputPeriode_r "$PMTK001,220,3*30\r\n\0"
#define l80r_nmeaOn_r "$PMTK001,314,3*36\r\n\0"

/* HAL Peripheral Handler */
extern UART_HandleTypeDef UART_HANDLER;
extern TIM_HandleTypeDef TIMER_HANDLER;
extern DMA_HandleTypeDef DMATX_HANDLER;
extern DMA_HandleTypeDef DMARX_HANDLER;

/* Private variable */
uint8_t gps_rx_buffer[GPS_BUFFER_SIZE] = {0};
gps_data_t internal_gpsData = {0};
typedef struct
{
	volatile uint8_t gps_status;
	volatile uint8_t dma_status;
	volatile uint8_t init_seqNumber;
	volatile uint8_t pmtk_commandLen;
	volatile uint8_t reply_commandLen;
} l80r_initData_t;
l80r_initData_t l80r_initData =
{
	.gps_status = L80R_UNINITIALIZE,
	.dma_status = L80R_DMADEFAULTMODE,
	.init_seqNumber = 0,
	.pmtk_commandLen = 0,
	.reply_commandLen = 0
};

/* Private function prototypes */
void L80R_DMA_TxMode();
void L80R_Set_Timer(uint16_t periode);
void L80R_ReInit();
void L80R_Reset_State();
void L80R_Parse(uint8_t* rx_buffer);
uint8_t* L80R_Get_InitCommand(uint8_t seq_numb);
uint8_t* L80R_Get_ReplyCommand(uint8_t seq_numb);

/**
  * @brief  GPS begin initialization.
  * @param  None.
  * @retval None.
  */
void L80R_Init()
{
	/* Set GPS status */
	l80r_initData.gps_status = L80R_UNINITIALIZE;

	/* Set DMA status */
	l80r_initData.dma_status = L80R_DMADEFAULTMODE;

	/* Set counter limit to make interrupt exactly 5 seconds  */
	L80R_Set_Timer(1000);

	/* Clear Timer Update Interrupt Flag (UIF) */
	__HAL_TIM_CLEAR_FLAG(&TIMER_HANDLER, TIM_FLAG_UPDATE);

	/* Start Timer */
	HAL_TIM_Base_Start_IT(&TIMER_HANDLER);

	/* UART IDLE Flag occurs */
	if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_IDLE))
	{
		/* Clear UART IDLE Flag */
		__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_IDLEF);
	}

	/* Enable UART IDLE Interrupt */
	__HAL_UART_ENABLE_IT(&UART_HANDLER, UART_IT_IDLE);

	/* Turn on GPS */
	// GPS turn on code here
}

/**
  * @brief  GPS UART callback. This function should be called when interrupt at UART occurs.
  * @param  None.
  * @retval None.
  */
void L80R_UART_Callback()
{
	/* UART IDLE Interrupt and no UART Errors occurs */
	if((__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_IDLE)) && (UART_HANDLER.ErrorCode == 0))
	{
		/* Disable Timers */
		HAL_TIM_Base_Stop_IT(&TIMER_HANDLER);

		/* Reset Timer counter */
		__HAL_TIM_SET_COUNTER(&TIMER_HANDLER, 0);

		/* GPS has been initialized */
		if(l80r_initData.gps_status == L80R_RUNNING)
		{
			/* Reset Timer counter */
			__HAL_TIM_SET_COUNTER(&TIMER_HANDLER, 0);

			/* Stop UART DMA */
			HAL_UART_DMAStop(&huart1);

			/* Parsing GPS data */
			L80R_Parse(&gps_rx_buffer[0]);

			/* Check IDLE Flag */
			if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_IDLE))
			{
				/* Clear IDLE Flag */
				__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_IDLEF);
			}

			/* Overrun Flag is set */
			if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_ORE))
			{
				/* Clear Overrun Error Flag */
				__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_OREF);

				/* Flush RX registry */
				__HAL_UART_FLUSH_DRREGISTER(&UART_HANDLER);
			}

			/* Start receiving data */
			HAL_UART_Receive_DMA(&UART_HANDLER, &gps_rx_buffer[0], 199);
		}
		else if(l80r_initData.gps_status == L80R_READY)
		{
			/* Change GPS status */
			l80r_initData.gps_status = L80R_RUNNING;

			/* Start Timer */
			__HAL_TIM_SET_COUNTER(&TIMER_HANDLER, 0);

			/* IDLE Flag is set */
			if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_IDLE))
			{
				/* Clear IDLE Flag */
				__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_IDLEF);
			}

			/* Overrun Flag is set */
			if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_ORE))
			{
				/* Clear Overrun Error Flag */
				__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_OREF);

				/* Flush RX registry */
				__HAL_UART_FLUSH_DRREGISTER(&UART_HANDLER);
			}

			/* Start receiving data */
			HAL_UART_Receive_DMA(&UART_HANDLER, &gps_rx_buffer[0], 199);
		}
		else if((l80r_initData.gps_status == L80R_INITIALIZATION) && (l80r_initData.dma_status == L80R_DMADEFAULTMODE))
		{
			/* Disable IDLE Interrupt */
			__HAL_UART_DISABLE_IT(&UART_HANDLER, UART_IT_IDLE);

			/* UART IDLE Flag occurs */
			if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_IDLE))
			{
				/* Clear IDLE flag */
				__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_IDLEF);
			}

			/* Bootstrap for the first transmit */
			L80R_DMA_TxMode();
		}
		else if((l80r_initData.gps_status == L80R_UNINITIALIZE))
		{
			/* Clear IDLE Flag */
			__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_IDLEF);

			/* Set USART current state */
			l80r_initData.gps_status = L80R_INITIALIZATION;

			/* Start Timer */
			HAL_TIM_Base_Start_IT(&TIMER_HANDLER);
		}
	}
	else if(l80r_initData.gps_status == L80R_ERROR)
	{
		/* Change GPS status */
		l80r_initData.gps_status = L80R_INITIALIZATION;

		/* Retransmit PMTK Command that correspond to current sequence number */
		L80R_DMA_TxMode();
	}
}

/**
  * @brief  GPS DMA transmit handler. This function transmit PMTK command in DMA mode to begin initialization process.
  * @param  None.
  * @retval None.
  */
void L80R_DMA_TxMode()
{
	/* Change DMA status */
	l80r_initData.dma_status = L80R_DMATXMODE;

	/* Get PMTK Command that correspond to current sequence number */
	uint8_t* l80r_pmtkCommand = L80R_Get_InitCommand(l80r_initData.init_seqNumber);

	/* Get PMTK Command length */
	l80r_initData.pmtk_commandLen = strlen((char*) l80r_pmtkCommand);

	/* Get PMTK Reply length */
	l80r_initData.reply_commandLen = strlen((char*) L80R_Get_ReplyCommand(l80r_initData.init_seqNumber));

	/* Set counter limit to make interrupt exactly 1 seconds  */
	L80R_Set_Timer(200);

	/* Clear Update Interrupt Flag (UIF) */
	__HAL_TIM_CLEAR_FLAG(&TIMER_HANDLER, TIM_FLAG_UPDATE);

	/* Start Timer */
	HAL_TIM_Base_Start_IT(&TIMER_HANDLER);

	/* Overrun Flag is set */
	if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_ORE))
	{
		/* Clear Overrun Error */
		__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_OREF);

		/* Flush RX registry */
		__HAL_UART_FLUSH_DRREGISTER(&UART_HANDLER);
	}

	/* Transmit through USART via DMA */
	if(HAL_UART_Transmit_DMA(&UART_HANDLER, l80r_pmtkCommand, l80r_initData.pmtk_commandLen) != HAL_OK)
	{
		__NOP();
	}
}

/**
  * @brief  GPS DMA receive handler. This function start the UART DMA receive function.
  * @param  None.
  * @retval None.
  */
void L80R_DMA_RxMode()
{
	/* Transmit Complete Interrupt occurs and current DMA status is L80R_DMATXMODE */
	if(__HAL_DMA_GET_FLAG(&DMATX_HANDLER, DMA_FLAG_TC4) && (l80r_initData.dma_status == L80R_DMATXMODE))
	{
		/* Reset Timer counter */
		__HAL_TIM_SET_COUNTER(&TIMER_HANDLER, 0);

		/* Change DMA status */
		l80r_initData.dma_status = L80R_DMARXMODE;

		/* Overrun Flag is set */
		if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_ORE))
		{
			/* Clear Overrun Error */
			__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_OREF);

			/* Flush RX registry */
			__HAL_UART_FLUSH_DRREGISTER(&UART_HANDLER);
		}

		/* Receive through USART via DMA */
		if(HAL_UART_Receive_DMA(&UART_HANDLER, &gps_rx_buffer[0], l80r_initData.reply_commandLen) != HAL_OK)
		{
			__NOP();
		}
	}
}

/**
  * @brief  Proceed received PMTK reply. This function proceed reply from corresponding PMTK command.
  * @param  None.
  * @retval None.
  */
void L80R_UART_RxProcess()
{
	/* Transmit Complete (TC) Interrupt occurs and DMA status is L80R_DMARXMODE */
	if(__HAL_DMA_GET_FLAG(&DMARX_HANDLER, DMA_FLAG_TC5) && (l80r_initData.dma_status == L80R_DMARXMODE))
	{
		/* Stop Timer */
		HAL_TIM_Base_Stop_IT(&TIMER_HANDLER);

		/* Get expected reply from previous PMTK Command */
		uint8_t* expected_reply = L80R_Get_ReplyCommand(l80r_initData.init_seqNumber);

		/* Compare current received reply with expected reply */
		if((strcmp((const char*) &gps_rx_buffer[0], (const char*) expected_reply) == 0))
		{
			/* Increment initialization sequence number to make next DMA transmit using next initialization PMTK command */
			if(++l80r_initData.init_seqNumber > 4)
			{
				/* Initializing completed */
				/* Change DMA status */
				l80r_initData.dma_status = L80R_DMADEFAULTMODE;

				/* Change GPS status */
				l80r_initData.gps_status = L80R_READY;

				/* Clear PMTK command length */
				l80r_initData.pmtk_commandLen = 0;

				/* Clear Reply PMTK command length */
				l80r_initData.reply_commandLen = 0;

				/* Clear RX buffer */
				memset(&gps_rx_buffer[0], 0x00, 60);

				/* Set counter limit to make interrupt exactly 0.75 seconds (Data being transmitted every 600ms from GPS module) */
				L80R_Set_Timer(150);

				/* Clear Update Interrupt Flag (UIF) */
				__HAL_TIM_CLEAR_FLAG(&TIMER_HANDLER, TIM_FLAG_UPDATE);

				/* Start Timer */
				HAL_TIM_Base_Start_IT(&TIMER_HANDLER);

				/* Overrun Flag is set */
				if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_ORE))
				{
					/* Clear Overrun Error */
					__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_OREF);

					/* Flush RX registry */
					__HAL_UART_FLUSH_DRREGISTER(&UART_HANDLER);
				}

				/* IDLE flag is set */
				if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_IDLE))
				{
					/* Clear IDLE flag */
					__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_IDLEF);
				}

				/* IDLE Interrupt is not enabled */
				if(!__HAL_UART_GET_IT_SOURCE(&UART_HANDLER, UART_IT_IDLE))
				{
					/* Enable IDLE Interrupt */
					__HAL_UART_ENABLE_IT(&UART_HANDLER, UART_IT_IDLE);
				}
			}
			else
			{
				/* Clear RX buffer */
				memset(&gps_rx_buffer[0], 0x00, 60);

				/* Send next initi PMTK command */
				L80R_DMA_TxMode();
			}
		}
		else
		{
			/* Prepare for reinitialize */
			L80R_Reset_State();

			/* Reinitialize GPS */
			L80R_ReInit();
		}
	}
}

/**
  * @brief  Parse received data in RX buffer.
  * @param  rx_buffer Pointer to first element of RX buffer.
  * @retval None.
  */
void L80R_Parse(uint8_t* rx_buffer)
{
	/* Debug LED */
	HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_SET);

	/* Required local variable */
	uint8_t* RMC = NULL;
	uint8_t* GGA = NULL;
	uint8_t RMC_Matched = 0;
	uint8_t GGA_Matched = 0;

	/* Search for RMC and GGA sentence */
	RMC = (uint8_t*) strstr((const char*) rx_buffer, "$GPRMC");
	GGA = (uint8_t*) strstr((const char*) rx_buffer, "$GPGGA");

	/* RMC sentence found */
	if(RMC != NULL)
	{
		/* Parse the data and record how many section that successfully parsed */
		RMC_Matched = sscanf((const char*) RMC, "$GPRMC,%2hhu%2hhu%2hhu.%hu,%c,%lf,%c,%lf,%c,%f,%f,%2hhu%2hhu%2hhu,,,%c*%hhx\r\n",
				&internal_gpsData.rmc_data.hours,
				&internal_gpsData.rmc_data.minutes,
				&internal_gpsData.rmc_data.seconds,
				&internal_gpsData.rmc_data.microseconds,
				&internal_gpsData.rmc_data.valid,
				&internal_gpsData.rmc_data.latitude,
				&internal_gpsData.rmc_data.ns,
				&internal_gpsData.rmc_data.longitude,
				&internal_gpsData.rmc_data.ew,
				&internal_gpsData.rmc_data.speed,
				&internal_gpsData.rmc_data.heading,
				&internal_gpsData.rmc_data.date,
				&internal_gpsData.rmc_data.month,
				&internal_gpsData.rmc_data.year,
				&internal_gpsData.rmc_data.positioning_mode,
				&internal_gpsData.rmc_data.checksum
				);
	}
	if(GGA != NULL)
	{
		GGA_Matched = sscanf((const char*) GGA, "$GPGGA,%2hhu%2hhu%2hhu.%hu,%lf,%c,%lf,%c,%hhu,%hhu,%lf,%lf,%*c,%lf,%*c,%*hu,%*hu*%hhx\r\n",
				&internal_gpsData.gga_data.hours,
				&internal_gpsData.gga_data.minutes,
				&internal_gpsData.gga_data.seconds,
				&internal_gpsData.gga_data.microseconds,
				&internal_gpsData.gga_data.latitude,
				&internal_gpsData.gga_data.ns,
				&internal_gpsData.gga_data.longitude,
				&internal_gpsData.gga_data.ew,
				&internal_gpsData.gga_data.fix_status,
				&internal_gpsData.gga_data.satellite_view,
				&internal_gpsData.gga_data.hdop,
				&internal_gpsData.gga_data.altitude,
				//&internal_gpsData.gga_data.alt_unit,
				&internal_gpsData.gga_data.geoid_separation,
				//&internal_gpsData.gga_data.geoid_unit,
				//&internal_gpsData.gga_data.dgps_age,
				//&internal_gpsData.gga_data.geoid_unit,
				&internal_gpsData.gga_data.checksum
				);
	}

	/* Clear GPS buffer */
	memset(&gps_rx_buffer[0], 0x00, 200);

	/* Debug LED */
	HAL_GPIO_WritePin(LD4_GPIO_Port, LD4_Pin, GPIO_PIN_RESET);
}

/**
  * @brief  Set Timer time length. This function is used to change time length of Timer before firing interrupt.
  * @param  periode Count limit for make specified time before interrupt.
  * @retval None.
  */
void L80R_Set_Timer(uint16_t periode)
{
	/* Deinitialize Timer to change the Auto Reload Register (ARR) */
	HAL_TIM_Base_DeInit(&TIMER_HANDLER);

	/* Set counter limit to make interrupt */
	/* 200 Hz internal clock */
	TIMER_HANDLER.Init.Period = periode;

	/* Reinitialize the Timer */
	HAL_TIM_Base_Init(&TIMER_HANDLER);
}

/**
  * @brief  UART Error handler. Internally called by HAL Drivers
  * @param  None.
  * @retval None.
  */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	/* Change GPS status */
	l80r_initData.gps_status = L80R_ERROR;

	/* Stop Timer */
	HAL_TIM_Base_Stop_IT(&TIMER_HANDLER);

	/* Abort all UART Interrupt */
	HAL_UART_Abort_IT(&UART_HANDLER);

	/* Stop DMA */
	HAL_UART_DMAStop(&UART_HANDLER);

	/* Reset Timer counter */
	__HAL_TIM_SET_COUNTER(&TIMER_HANDLER, 0);
}

/**
  * @brief  Timers Update Interrupt Callback. Internally called by HAL Drivers
  * @param  None.
  * @retval None.
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/* Timeout before IDLE Frame detected */
	if(((l80r_initData.gps_status == L80R_INITIALIZATION) && (l80r_initData.dma_status == L80R_DMADEFAULTMODE)) || l80r_initData.gps_status == L80R_UNINITIALIZE)
	{
		/* Reinitialize GPS */
		L80R_ReInit();
	}
	else if(((l80r_initData.gps_status == L80R_INITIALIZATION) && (l80r_initData.dma_status != L80R_DMADEFAULTMODE)) || l80r_initData.gps_status == L80R_RUNNING)
	{
		/* Prepare for reInit */
		L80R_Reset_State();

		/* Reinit USART to capture next IDLE line */
		L80R_ReInit();
	}
}

/**
  * @brief  Restart initialization.
  * @param  None.
  * @retval None.
  */
void L80R_ReInit()
{
	/* Change GPS status */
	l80r_initData.gps_status = L80R_UNINITIALIZE;

	/* Set counter limit to make interrupt exactly 5 seconds  */
	L80R_Set_Timer(1000);

	/* Clear Update Interrupt Flag (UIF) */
	__HAL_TIM_CLEAR_FLAG(&TIMER_HANDLER, TIM_FLAG_UPDATE);

	/* Start Timer */
	HAL_TIM_Base_Start_IT(&TIMER_HANDLER);

	/* IDLE Flag is set */
	if(__HAL_UART_GET_FLAG(&UART_HANDLER, UART_FLAG_IDLE))
	{
		/* Clear IDLE Flag */
		__HAL_UART_CLEAR_FLAG(&UART_HANDLER, UART_CLEAR_IDLEF);
	}

	/* IDLE Interrupt is not enabled */
	if(!__HAL_UART_GET_IT_SOURCE(&UART_HANDLER, UART_IT_IDLE))
	{
		/* Enable IDLE Interrupt */
		__HAL_UART_ENABLE_IT(&UART_HANDLER, UART_IT_IDLE);
	}

	/* Restart GPS */
	// GPS restart code here
}

/**
  * @brief  Reset GPS state to initialization.
  * @param  None.
  * @retval None.
  */
void L80R_Reset_State()
{
	/* Abort UART IT */
	HAL_UART_Abort_IT(&UART_HANDLER);

	/* Disable DMA */
	HAL_UART_DMAStop(&UART_HANDLER);

	/* Stop Timer */
	HAL_TIM_Base_Stop_IT(&TIMER_HANDLER);

	/* Change DMA status to default */
	l80r_initData.dma_status = L80R_DMADEFAULTMODE;

	/* Restart sequence number */
	l80r_initData.init_seqNumber = 0;

	/* Clear PMTK Command length */
	l80r_initData.pmtk_commandLen = 0;

	/* Clear Reply PMTK Command length */
	l80r_initData.reply_commandLen = 0;

	/* Clear RX buffer */
	memset(&gps_rx_buffer[0], 0x00, 200);
}

/**
  * @brief  Get PMTK command. This function get string of PMTK init command corresponding to init sequence number.
  * @param  seq_number Initialization tracker number.
  * @retval None.
  */
uint8_t* L80R_Get_InitCommand(uint8_t seq_numb)
{
	/* Sequence of PMTK Command */
	uint8_t* l80r_listCommand[5] =
	{
		(uint8_t*) &l80r_nmeaOff[0], (uint8_t*) &l80r_normalMode[0], (uint8_t*) &l80r_easyMode[0], (uint8_t*) &l80r_setOutputPeriode[0], (uint8_t*) &l80r_nmeaOn[0]
	};

	/* Send back PMTK Command that correspond to current sequence number */
	return l80r_listCommand[seq_numb];
}

/**
  * @brief  Get PMTK reply that corresponding to PMTK command.
  * @param  seq_numb Initialization tracker number.
  * @retval None.
  */
uint8_t* L80R_Get_ReplyCommand(uint8_t seq_numb)
{
	/* Reply sequence of PMTK Command */
	uint8_t* l80r_listReply[5] =
	{
		(uint8_t*) &l80r_nmeaOff_r[0], (uint8_t*) &l80r_normalMode_r[0], (uint8_t*) &l80r_easyMode_r[0], (uint8_t*) &l80r_setOutputPeriode_r[0], (uint8_t*) &l80r_nmeaOn_r[0]
	};

	/* Send back PMTK Command Reply that correspond to current sequence number */
	return l80r_listReply[seq_numb];
}
